﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SlideNote
{
    public partial class Main : Form
    {

        private List<Button> buttons = new List<Button>();
        private int speed = 10;
        private int border = 32;
        private bool mouse = false;
        private bool textChanged = false;

        public Main() {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e) {

            if (String.IsNullOrEmpty(Properties.Settings.Default.NotesFolder) || String.IsNullOrEmpty(Properties.Settings.Default.NotesPrefix)) {
                Properties.Settings.Default.NotesFolder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Notes";
                Properties.Settings.Default.NotesPrefix = "Note_";
                Properties.Settings.Default.Save();
            }

            this.Left = Screen.PrimaryScreen.WorkingArea.Width - border;
            this.ShowInTaskbar = false;
            for (int i = 0; i < 9; i++) { 
				Button button = new Button();
				button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
				button.Location = new System.Drawing.Point(2, 37+24*i);
				button.Name = "button"+i.ToString();
				button.Size = new System.Drawing.Size(18, 21);
				button.TabIndex = 10;
				button.Text = (i+1).ToString();
                button.UseVisualStyleBackColor = true;
                button.Click += new System.EventHandler(tab_Click);
                this.Controls.Add(button);

                var text = "";
                try { 
					text = File.ReadAllText(Properties.Settings.Default.NotesFolder+"\\"+Properties.Settings.Default.NotesPrefix + i.ToString() + ".txt");
                }catch(Exception ex){} 
                TextBox textbox = new TextBox();
                textbox.Location = new System.Drawing.Point(22, 13);
                textbox.Multiline = true;
                textbox.Name = "textBox"+i.ToString();
                textbox.Visible = false;
                textbox.Text = text;
                textbox.Size = new System.Drawing.Size(306, 237);
                textbox.TabIndex = 1;
                textbox.TextChanged += new System.EventHandler(textChange);
                this.Controls.Add(textbox);
            }
			tab_Click((Button)Controls.Find("button0", false)[0],null);
        }

        private void tab_Click(object sender, EventArgs e)
        {
			string current = (Int32.Parse(((Button)sender).Text)-1).ToString();
            for (int i = 0; i < 9; i++)
            {
                ((Button)Controls.Find("button" + i.ToString(),false)[0]).BackColor = SystemColors.Control;
                ((TextBox)Controls.Find("textBox" + i.ToString(),false)[0]).Visible = false;
            }
			((Button)Controls.Find("button" +current ,false)[0]).BackColor = SystemColors.ActiveCaption;
			((TextBox)Controls.Find("textBox" + current,false)[0]).Visible = true;
        }

        private void textChange(object sender, EventArgs e)
        {
            textChanged = true;
        }

        private void show() { 
            if (this.Right > Screen.PrimaryScreen.WorkingArea.Width +15) {
                this.Left-=speed;
            }
        }
        private void hide() {
            if (this.Left < Screen.PrimaryScreen.WorkingArea.Width-border) {
                this.Left+=speed;
            }
        }

        private void moveTimer_Tick(object sender, EventArgs e) {
            if (this.ClientRectangle.Contains(this.PointToClient(Control.MousePosition)))
                mouse = true;
            else 
                mouse = false;

            if (mouse)
                show();
            else
                hide();
        }

        private void Form1_Deactivate(object sender, EventArgs e) {
            mouse = false;
        }

        private void saveTimer_Tick(object sender, EventArgs e) {
            if (!textChanged)
                return;

            System.IO.Directory.CreateDirectory(Properties.Settings.Default.NotesFolder);
            for (int i = 0; i < 9; i++)
            {
                string text = ((TextBox)Controls.Find("textBox" + i.ToString(), false)[0]).Text;
                if (!String.IsNullOrEmpty(text))
					File.WriteAllText(Properties.Settings.Default.NotesFolder+"\\"+Properties.Settings.Default.NotesPrefix +i.ToString()+".txt", text);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            (new Settings()).Show();
        }
    }
}
