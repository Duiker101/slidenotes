﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SlideNote
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            textBox1.Text = Properties.Settings.Default.NotesFolder;
            textBox2.Text = Properties.Settings.Default.NotesPrefix;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.NotesFolder = textBox1.Text;
            Properties.Settings.Default.NotesPrefix = textBox2.Text;
			Properties.Settings.Default.Save();
            Close();
        }
    }
}
